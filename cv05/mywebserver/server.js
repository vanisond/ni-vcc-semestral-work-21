const http = require('http');
const port = process.env.PORT || 8080;

const server = http.createServer(async function(req, res) {
    const found = req.url.match(/^\/([A-Za-z]+)$/)
    if (!found) {
        res.writeHead(400);
        res.end();
        return;
    }
    const name = found[1];
    const responseText = `Hello ${name}!`;
    res.writeHead(200, {'Content-Type': 'text/plain; charset=utf-8'});
    res.end(responseText);
});

server.listen(port, function() {
    console.log(`Mock server started on port ${port}.`);
});
